package ru.kolesnikov.tm.exception.system;

import ru.kolesnikov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String arg) {
        super("Error! Unknown argument...");
    }

}