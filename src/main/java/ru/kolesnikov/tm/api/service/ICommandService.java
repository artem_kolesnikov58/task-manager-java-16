package ru.kolesnikov.tm.api.service;

import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommandList();

}