package ru.kolesnikov.tm.api.service;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

}