package ru.kolesnikov.tm.api.repository;

import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommandList();

}