package ru.kolesnikov.tm.bootstrap;

import ru.kolesnikov.tm.api.repository.*;
import ru.kolesnikov.tm.api.service.*;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.exception.system.UnknownArgumentException;
import ru.kolesnikov.tm.exception.system.UnknownCommandException;
import ru.kolesnikov.tm.repository.CommandRepository;
import ru.kolesnikov.tm.repository.ProjectRepository;
import ru.kolesnikov.tm.repository.TaskRepository;
import ru.kolesnikov.tm.repository.UserRepository;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.service.*;
import ru.kolesnikov.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        initCommands(commandService.getCommandList());
    }

    private void initCommands(final List<AbstractCommand> commandList) {
        for (AbstractCommand command : commandList) init(command);
    }

    private void init(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.commandName(), command);
        arguments.put(command.arg(), command);
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        authService.checkRoles(command.roles());
        command.execute();
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        argument.execute();
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IAuthService getAuthService() {
        return authService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

}