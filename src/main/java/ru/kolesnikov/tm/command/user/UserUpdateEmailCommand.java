package ru.kolesnikov.tm.command.user;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.TerminalUtil;

public class UserUpdateEmailCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "user-update-email";
    }

    @Override
    public String description() {
        return "Update user e-mail.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER E-MAIL]");
        System.out.println("ENTER NEW USER E-MAIL:");
        final String newEmail = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserEmail(userId, newEmail);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}