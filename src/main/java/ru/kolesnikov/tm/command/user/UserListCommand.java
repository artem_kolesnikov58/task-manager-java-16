package ru.kolesnikov.tm.command.user;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.enumerated.Role;

import java.util.List;

public class UserListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "show-user-list";
    }

    @Override
    public String description() {
        return "Show user list.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        final List<User> users = serviceLocator.getUserService().findAll();
        if (users == null) {
            System.out.println("[FAIL]");
            return;
        }
        int index = 1;
        for (User user : users) {
            System.out.println(index + ". " + user.getLogin());
            index++;
        }

    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}