package ru.kolesnikov.tm.command.user;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.TerminalUtil;

public class UserUpdateLastNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "user-update-last-name";
    }

    @Override
    public String description() {
        return "Update user last name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME:");
        final String newLastName = TerminalUtil.nextLine();
        serviceLocator.getAuthService().updateUserLastName(userId, newLastName);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}