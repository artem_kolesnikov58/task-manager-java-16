package ru.kolesnikov.tm.command.user;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.User;
import ru.kolesnikov.tm.enumerated.Role;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "view-profile";
    }

    @Override
    public String description() {
        return "View profile.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[VIEW USER PROFILE]");
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}