package ru.kolesnikov.tm.command;

import ru.kolesnikov.tm.api.service.ServiceLocator;
import ru.kolesnikov.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String arg();

    public abstract String commandName();

    public abstract String description();

    public abstract void execute();

}