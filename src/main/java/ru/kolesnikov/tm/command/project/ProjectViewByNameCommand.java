package ru.kolesnikov.tm.command.project;

import ru.kolesnikov.tm.command.ProjectAbstractCommand;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.enumerated.Role;
import ru.kolesnikov.tm.util.TerminalUtil;

public class ProjectViewByNameCommand extends ProjectAbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "project-view-by-name";
    }

    @Override
    public String description() {
        return "View project by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}