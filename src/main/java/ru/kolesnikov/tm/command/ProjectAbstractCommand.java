package ru.kolesnikov.tm.command;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.entity.Project;
import ru.kolesnikov.tm.enumerated.Role;

public abstract class ProjectAbstractCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER };
    }


}