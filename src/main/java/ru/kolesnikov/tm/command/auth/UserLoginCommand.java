package ru.kolesnikov.tm.command.auth;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "login";
    }

    @Override
    public String description() {
        return "Login user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}