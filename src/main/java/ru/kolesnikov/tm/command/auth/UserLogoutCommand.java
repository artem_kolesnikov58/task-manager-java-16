package ru.kolesnikov.tm.command.auth;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}