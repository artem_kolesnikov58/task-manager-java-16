package ru.kolesnikov.tm.command.auth;

import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.util.TerminalUtil;

public class UserRegistrationCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "registration";
    }

    @Override
    public String description() {
        return "Registration user.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[OK]");
    }

}