package ru.kolesnikov.tm.command.system;

import ru.kolesnikov.tm.bootstrap.Bootstrap;
import ru.kolesnikov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public String commandName() {
        return "commands";
    }

    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommandService().getCommandList();
        for (AbstractCommand command: commands) {
            System.out.println(command.commandName());
        }
        System.out.println("[OK]");
    }

}