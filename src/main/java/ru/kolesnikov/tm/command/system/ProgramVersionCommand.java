package ru.kolesnikov.tm.command.system;

import ru.kolesnikov.tm.command.AbstractCommand;

public final class ProgramVersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String commandName() {
        return "version";
    }

    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.16");
    }

}