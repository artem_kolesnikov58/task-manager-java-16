package ru.kolesnikov.tm.command.system;

import ru.kolesnikov.tm.command.AbstractCommand;

public final class ProgramExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "exit";
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
            System.exit(0);
        System.out.println("[OK]");
    }

}
