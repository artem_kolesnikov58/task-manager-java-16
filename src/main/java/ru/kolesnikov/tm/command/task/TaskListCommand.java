package ru.kolesnikov.tm.command.task;

import ru.kolesnikov.tm.command.TaskAbstractCommand;
import ru.kolesnikov.tm.entity.Task;
import ru.kolesnikov.tm.enumerated.Role;

import java.util.List;

public class TaskListCommand extends TaskAbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String commandName() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST]");
        final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        int index = 1;
        for (Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}