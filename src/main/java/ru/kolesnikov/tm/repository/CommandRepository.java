package ru.kolesnikov.tm.repository;

import ru.kolesnikov.tm.api.repository.ICommandRepository;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.command.auth.UserLoginCommand;
import ru.kolesnikov.tm.command.auth.UserLogoutCommand;
import ru.kolesnikov.tm.command.auth.UserRegistrationCommand;
import ru.kolesnikov.tm.command.project.*;
import ru.kolesnikov.tm.command.system.*;
import ru.kolesnikov.tm.command.task.*;
import ru.kolesnikov.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[]{
            ProgramHelpCommand.class, AboutCommand.class, ArgumentCommand.class, CommandCommand.class,
            ProgramExitCommand.class, ProgramVersionCommand.class, SystemInfoCommand.class,

            UserLoginCommand.class, UserLogoutCommand.class, UserRegistrationCommand.class,

            ProjectClearCommand.class, ProjectCreateCommand.class, ProjectListCommand.class,
            ProjectRemoveByIdCommand.class, ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class,
            ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class, ProjectViewByIdCommand.class,
            ProjectViewByIndexCommand.class, ProjectViewByNameCommand.class,

            TaskClearCommand.class, TaskCreateCommand.class, TaskListCommand.class, TaskRemoveByIdCommand.class,
            TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class, TaskUpdateByIdCommand.class,
            TaskUpdateByIndexCommand.class, TaskViewByIdCommand.class, TaskViewByIndexCommand.class,
            TaskViewByNameCommand.class,

            UserUpdateEmailCommand.class, UserUpdateFirstNameCommand.class, UserUpdateLastNameCommand.class,
            UserUpdateMiddleNameCommand.class, UserUpdatePasswordCommand.class, UserViewProfileCommand.class,

            UserListCommand.class, UserLockByLoginCommand.class, UserRemoveByLoginCommand.class,
            UserUnLockByLoginCommand.class,
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz: COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}