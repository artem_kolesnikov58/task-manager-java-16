package ru.kolesnikov.tm.service;

import ru.kolesnikov.tm.api.repository.ICommandRepository;
import ru.kolesnikov.tm.api.service.ICommandService;
import ru.kolesnikov.tm.command.AbstractCommand;
import ru.kolesnikov.tm.dto.Command;

import java.util.List;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}